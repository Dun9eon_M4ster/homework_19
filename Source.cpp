#include <iostream>
using namespace std;

class Animal
{
protected:
	string ExampleVoice = "Example voice";
public:
	void Voice()
	{
		cout << ExampleVoice << endl;
	}
};

class Dog : public Animal
{
private:
	string DogVoice = "Woof!";
public:
	Dog()
	{
		ExampleVoice = DogVoice;
	}
};

class Cat : public Animal
{
private:
	string CatVoice = "Meow!";
public:
	Cat()
	{
		ExampleVoice = CatVoice;
	}
};

class Mouse : public Animal
{
private:
	string MouseVoice = "Pi!";
public:
	Mouse()
	{
		ExampleVoice = MouseVoice;
	}
};

int main()
{
	Dog dog;
	Cat cat;
	Mouse mouse;
	dog.Voice();
	cat.Voice();
	mouse.Voice();

	cout << "---------------" << endl;

	Animal* ptr = new Animal[3];
	ptr[0] = dog;
	ptr[1] = cat;
	ptr[2] = mouse;

	for (int i = 0; i < 3; i++)
	{
		ptr[i].Voice();
	}

	return 0;
}